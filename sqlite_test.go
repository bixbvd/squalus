package squalus

import (
	"database/sql"
	"os"
	"testing"

	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/assert"
)

func ConnectToSQLite(t *testing.T) *DB {
	os.Remove("./test.db")
	db, err := sql.Open("sqlite3", "./test.db")
	if err != nil {
		t.Fatalf("Failed to connect to SQLite: %s", err)
	}
	err = db.Ping()
	if err != nil {
		t.Fatalf("Failed to connect to SQLite server: %s", err)
	}
	sdb, err := NewDB(db)
	if err != nil {
		t.Fatalf("Failed to create Squalus DB: %s", err)
	}
	sdb.Exec("drop table if exists [persons]", nil)
	_, err = sdb.Exec("create table [persons]([id] int, [name] varchar(128), [height] float, [birth] datetime)", nil)
	assert.NoError(t, err, "Table creation should succeed")

	sdb.Exec("drop table if exists testbinary", nil)
	_, err = sdb.Exec("create table testbinary([data] blob)", nil)
	assert.NoError(t, err, "Table creation should succeed")

	return sdb
}
