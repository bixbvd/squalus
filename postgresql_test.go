package squalus

import (
	"database/sql"
	"testing"

	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
)

func ConnectToPostgresql(t *testing.T) *DB {
	db, err := sql.Open("postgres", "user=u password=p dbname=db port=15432 sslmode=disable")
	errPing := db.Ping()
	if err != nil || errPing != nil {
		db, err = sql.Open("postgres", "user=u password=p dbname=db host=postgres port=5432 sslmode=disable")
		if err != nil {
			t.Fatalf("Failed to connect to Postgresql server: %s", err)
		}
	}
	err = db.Ping()
	if err != nil {
		t.Fatalf("Failed to connect to Postgresql server: %s", err)
	}
	sdb, err := NewDB(db)
	if err != nil {
		t.Fatalf("Failed to create Squalus DB: %s", err)
	}

	sdb.Exec("drop table if exists [persons]", nil)
	_, err = sdb.Exec("create table [persons]([id] int, [name] varchar(128), [height] float, [birth] timestamp with time zone)", nil)
	assert.NoError(t, err, "Table creation should succeed")

	_, err = sdb.Exec("set time zone 'UTC'", nil)
	assert.NoError(t, err, "Setting time zone should succeed")

	sdb.Exec("drop table if exists testbinary", nil)
	_, err = sdb.Exec("create table testbinary([data] bytea)", nil)
	assert.NoError(t, err, "Table creation should succeed")

	return sdb
}
