package squalus

import (
	"fmt"
	"reflect"
	"strings"
)

type postgresqlDriver struct{}

func (d postgresqlDriver) adaptQuery(query string, params map[string]interface{}) (string, []interface{}, error) {
	// replace [ and ] with "
	replacements := []string{`[`, `"`, `]`, `"`}
	pgQuery := strings.NewReplacer(replacements...).Replace(query)

	// TODO factor out the common code between this driver and the MySQL driver (trickier than it looks).

	infos := getParamsInfos(params)
	var pgArgs []interface{}
	re := paramsRegexp()

	// replace {args} with the appropriate $n
	rank := 1
	ranks := map[string]int{}
	missing := ""
	pgQuery = re.ReplaceAllStringFunc(
		pgQuery,
		func(name string) string {
			i, ok := infos[name]
			if !ok {
				if missing == "" {
					missing = name
				}
				return ""
			}
			r, already := ranks[name]
			if !already {
				r = rank
				ranks[name] = rank
				rank++
			}
			res := fmt.Sprintf("$%v", r)
			switch i.length {
			case 0:
				res = ""
			case 1:
				pgArgs = append(pgArgs, i.value)
			default:
				for j := 0; j < i.length; j++ {
					if j != 0 {
						res += fmt.Sprintf(",$%v", r+j)
					}
					if !already {
						rank++
						pgArgs = append(pgArgs, reflect.ValueOf(i.value).Index(j).Interface())
					}
				}
			}
			return res
		},
	)

	if missing != "" {
		return "", nil, fmt.Errorf("Parameter %s was not given a value", missing)
	}

	return pgQuery, pgArgs, nil
}
